<?php
$title = "Projekti | Fort-ing";
include 'header.php';

?>



<div class="projects-under-construction">
    <div class="construction-bager-container">
        <div class="construction-bager">
            <div class="object">
        
                <div class="object-rope"></div>
                <div class="object-shape">
                    Coming <span class="soon">Soon</span>
                </div>
            </div>
        </div>
    </div>

    <div class="content">


        <p class="message">Ova stranica je u izradi.</p>
        <button class="btn btn-primary primary border-primary" onclick="window.location.href='index.php';">Povratak</button>

        <p class="mailtoaddress push-top-25"><em>Email: info@forting.hr</em></p>
    </div>
</div>
<div class="construction d-none">
    <div class="project-fullscreen">
        <div class="project-fullscreen-image">
            <img class="project-fullscreen-real-image" src="./images/projekti/logo.png" alt="" srcset="">
            <p class="project-fullscreen-text text-primary">Ime slike bre</p>
            <button class="btn btn-primary primary border-primary btn-fullscreen">X</button>
        </div>

    </div>
    <div class="container-fluid ">

        <h1 class="push-top-75-small">Pronađite inspiraciju u našim projektima</h1>
        <H2 class="text-primary">bla bla bla</H2>
        <div class="project-carousel-container push-top-50">

            <div id="carouselExampleControls" class="carousel slide carousel-fade" data-bs-ride="carousel">
                <div class="carousel-inner ">
                    <div class="carousel-item active">
                        <div class="project-big-image-holder d-none d-md-block" style="background-image: url(./images/projekti/miramare.jpg);"></div>
                        <div class="project-small-card">
                            <div class="card">
                                <h3 class="order-sm-1 order-2">Hotel Miramare</h3>
                                <img class="card-img-top d-none d-md-block" src="images/projekti/miramare-bazen.jpg" alt="Bazen Hotel Miramare">
                                <img class="card-img-top d-sm-block d-md-none order-sm-2 order-1" src="images/projekti/miramare.jpg" alt="Hotel Miramare">
                                <div class="card-body order-sm-3 order-3">
                                    <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing
                                        elit. Sunt
                                        pariatur,
                                        obcaecati iste nostrum et officia. Provident quas, hic dolor, explicabo mollitia
                                        reiciendis,
                                        quos deserunt molestiae iusto dicta soluta sequi nam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="project-big-image-holder d-none d-md-block" style="background-image: url(./images/projekti/he-lesce.jpg);">
                        </div>
                        <div class="project-small-card">
                            <div class="card" style="">
                                <h3 class="order-sm-1 order-2">Hotel Miramare</h3>
                                <img class="card-img-top d-none d-md-block" src="images/projekti/he-lesce.jpg" alt="Hidroelektrana Lešće">
                                <img class="card-img-top d-sm-block d-md-none order-sm-2 order-1" src="images/projekti/he-lesce.jpg" alt="Hidroelektrana Lešće">
                                <div class="card-body order-sm-3 order-3">
                                    <p class="card-text">Neki drugi lorem ipsum, da bi se lakše vidjela promjena teksta.
                                        Bilo bi fora da postoji random lorem ipsum generator za ovakav slucaj.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="project-big-image-holder d-none d-md-block" style="background-image: url(./images/projekti/hotel-punta.jpg);">
                        </div>
                        <div class="project-small-card">
                            <div class="card" style="">
                                <h3 class="order-sm-1 order-2">Hotel Miramare</h3>
                                <img class="card-img-top d-none d-md-block" src="images/projekti/hotel-punta.jpg" alt="Hotel Punta">
                                <img class="card-img-top d-sm-block d-md-none order-sm-2 order-1" src="images/projekti/hotel-punta.jpg" alt="Hotel Punta">
                                <div class="card-body order-sm-3 order-3">
                                    <p class="card-text">Trebo bi isprogramirat custom ekstenziju za vs code koja
                                        generira
                                        random lorem ipsum, todo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-buttons">
                    <button class="carousel-control-prev btn btn-primary primary border-primary carousel-button projects-button" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev"><i class="bi bi-chevron-left"></i>
                        <span class="change-project-button">Prethodni projekt</span>
                    </button>
                    <button class="carousel-control-next btn btn-primary primary border-primary carousel-button projects-button" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                        <span class="change-project-button">Sljedeći projekt</span><i class="bi bi-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="project-carousel-small-container push-top-75">
        <h2 class="text-center">Galerija slika</h3>
            <div class="owl-carousel small-owl owl-theme push-top-50">
                <!-- <div class="project-carousel-small-image"><img class="gallery-image" src="images/projekti/miramare.jpg"
                        alt="" srcset=""></div>
                <div class="project-carousel-small-image"><img class="gallery-image"
                        src="images/projekti/miramare-bazen.jpg" alt="" srcset="">
                </div>
                <div class="project-carousel-small-image"><img class="gallery-image" src="images/projekti/hotel-punta.jpg"
                        alt="" srcset="">
                </div>
                <div class="project-carousel-small-image"><img class="gallery-image" src="images/projekti/he-lesce.jpg"
                        alt="" srcset=""></div>
                <div class="project-carousel-small-image"><img class="gallery-image" src="images/projekti/hotel-punta.jpg"
                        alt="" srcset="">
                </div> -->
            </div>
    </div>
</div>









<?php
include 'footer.php';
?>
<script>
    $(document).ready(function() {
        $('a[href^="projekti.php"]').addClass('active');
    });
</script>