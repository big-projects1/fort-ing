<div class="footer-container container-fluid text-white push-top-75">
    <div class="row footer-row">
        <div class="col-s-12 col-md-4 col-lg-4">
            <h3>Fort-ing j.d.o.o.</h3>
            <p>Vukovarska 50
                <br>
                51265 Dramalj, Croatia
                <br>
                OIB: 84209054678
                <br>
                Nadležni trgovački sud: Trgovački sud u Rijeci
                <br>
                Ovlašteni predstavnik društva: Domagoj Domijan
                <br>
                Račun: Erste &Steiermarkische Bank d.d.
                <br>
                IBAN:  HR5023400091100015801 
            </p>
        </div>
        <div class="col-s-12 col-md-4 col-lg-4 mx-auto order-3 order-md-2">
            <div class="footer-logo-container"><img src="./images/logo-fort-ing-new.png" class="footer-logo" alt="Logo Fort-ing"
                    srcset=""></div>

        </div>
        <div class="col-s-12 col-md-4 col-lg-4 mx-auto max-width order-2 order-md-3">
            <div class="float-right">
                <div class="footer-nav push-bottom-25 d-none d-md-block">
                    <a href="index.php" class="text-white">O nama</a>
                    &nbsp;•&nbsp;
                    <a href="usluge/inzenjer-gradilista.php" class="text-white">Usluge</a>
                    &nbsp;•&nbsp;
                    <a href="projekti.php" class="text-white">Projekti</a>
                    &nbsp;•&nbsp;
                    <a href="kontakt.php" class="text-white">Kontakt</a>
                </div>
                <p>Email: <a href="mailto:info@forting.hr">info@forting.hr</a> 
                    <br>
                    Telefon: <a href="tel:+385 (0)98 407 636">+385 (0)98 407 636</a>
                </p>
            </div>
        </div>

    </div>



    <div class="footer-nav push-bottom-25 d-sm-block d-md-none">
        <a href="index.php" class="text-white">O nama</a>
        •
        <a href="usluge/inzenjer-gradilista.php" class="text-white">Usluge</a>
        •
        <a href="projekti.php" class="text-white">Projekti</a>
        •
        <a href="kontakt.php" class="text-white">Kontakt</a>
    </div>

    <p class="text-center text-small">© Fort-ing j.d.o.o. - 2023.</p>
</div>



<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
</script>
<script src="js/scripts.js"></script>
<script src="owlcarousel/dist/owl.carousel.min.js"></script>
</body>

</html>