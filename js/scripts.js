function checkWindowSizeAndChangeLogo() {
  var navLogo = $(".navbar-logo");

  if ($(window).width() <= 992) {
    navLogo.attr("src", "./images/logo-F.png");
  } else {
    navLogo.attr("src", "./images/fort-ing-white.png");
  }
}

function bindBrickColorChange() {
  console.log($("rect"));
  var rekts = $("rect");
  var isFilled = false;
  for (let i = 0; i < rekts.length; i++) {
    $(rekts[i])
      .on("mouseenter", (e) => {
        if ($(e.target).attr("fill") == "#ED1E1E") {
          isFilled = true;
          $(e.target).attr("fill", "none");
        } else {

          isFilled = false;
          $(e.target).attr("fill", "#ED1E1E");
          // $(e.target).attr("width", "116.993");
          // $(e.target).attr("height", "43.0406");
        }
        
      })
      .on("mouseleave", (e) => {
        if (!isFilled) {
          $(e.target).attr("fill", "none");
          // $(e.target).attr("width", "106.993");
          // $(e.target).attr("height", "40.0406");
        }
        else{
          $(e.target).attr("fill", "#ED1E1E");
        }
       
      });
  }
}

$(document).ready(function () {
  bindBrickColorChange();
  checkWindowSizeAndChangeLogo();
  $(window).on("resize", () => {
    checkWindowSizeAndChangeLogo();
  });

  $(".owl-carousel-index").owlCarousel({
    loop: true,
    margin: 100,
    responsiveClass: true,
    navText: [
      '<i class="bi bi-chevron-left"></i>',
      '<i class="bi bi-chevron-right"></i>',
    ],
    dots: false,

    responsive: {
      0: {
        items: 1,
        nav: false,
        pagination: false,
        dots: true,
        loop: false,
      },
      768: {
        items: 2,
        nav: true,
        margin: 20,
      },
      992: {
        items: 3,
        nav: true,

        margin: 20,
        mouseDrag: false,
        touchDrag: false,
      },
      1200: {
        items: 3,
        nav: true,
        margin: 30,
        mouseDrag: false,
        touchDrag: false,
      },
      1630: {
        items: 3,
        nav: true,
        margin: 50,
        mouseDrag: false,
        touchDrag: false,
      },
      2560: {
        items: 3,
        nav: true,
        margin: 40,
        loop: true,
        mouseDrag: false,
        touchDrag: false,
      },
    },
  });
});
var smallCarousel = $(".small-owl");
var innerhtml = "";
var images = [];
var imageId = 0;

$(document).ready(function () {
  // Projekti fullscreen
  let id = 0;

  $.ajax({
    url: "images/projekti",
    success: function (data) {
      $(data)
        .find("a:contains(.jpg), a:contains(.png)")
        .each(function () {
          // will loop through
          var image = $(this).attr("href");
          innerhtml += `<div class='project-carousel-small-image'><img class='gallery-image'  id = '${id}'  src='images/projekti/${image}' alt='' srcset=''></div>`;
          images.push(image);
          id += 1;
        });
    },
  }).then(() => {
    smallCarousel.html(innerhtml);
    var start = 0;

    $(".btn-fullscreen").on("click", function () {
      $(".project-fullscreen").css("display", "none");
    });
    $(".small-owl").owlCarousel({
      loop: true,
      nav: false,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: false,
          stagePadding: 50,
          pagination: false,
          margin: 10,
        },
        768: {
          items: 2,
          stagePadding: 50,
          margin: 20,
        },
        992: {
          items: 3,
          stagePadding: 50,
          margin: 20,
        },
        1200: {
          items: 3,
          stagePadding: 50,
          margin: 30,
        },
        1630: {
          items: 3,
          margin: 100,
        },
        2560: {
          items: 4,
          margin: 80,
          loop: true,
        },
      },
    });

    $(document).on("mousedown", ".gallery-image", function () {
      start = Date.now();
      // Do stuff, get id of image perhaps?
    });
    $(document).on("mouseup", ".gallery-image", function () {
      var id = parseInt($(this).attr("id"));
      imageId = id;
      if (Date.now() - start < 100) {
        let imgName = images[id].replace("-", " ").split(".")[0];
        imgName = imgName.charAt(0).toUpperCase() + imgName.slice(1);
        $(".project-fullscreen").css("display", "flex");
        $(".project-fullscreen-text").html(`${imgName}`);
        $(".project-fullscreen-real-image").attr(
          "src",
          `./images/projekti/${images[id]}`
        );
      }
    });
    $(".owl-carousel-index").on("click", () => {
      if ($(window).width() >= 992) {
        var activeItems = $(".owl-carousel-index .owl-item.active");

        $(".owl-carousel-index .owl-item").css("transform", "scale(0.5)");
        $(".owl-carousel-index .owl-item").css("filter", "grayscale(1)");
        $(".owl-carousel-index .owl-item").css("opacity", "0.5");

        $(activeItems[0]).css("transform", "scale(0.8)");

        $(activeItems[1]).css("transform", "none");
        $(activeItems[1]).css("filter", "none");
        $(activeItems[1]).css("opacity", "1");

        $(activeItems[2]).css("transform", "scale(0.8)");
      }
    });
    if ($(window).width() >= 992) {
      var activeItems = $(".owl-carousel-index .owl-item.active");
      $(activeItems[0]).css("transform", "scale(0.8)");
      $(activeItems[0]).css("filter", "grayscale(1)");
      $(activeItems[0]).css("opacity", "0.5");
      $(activeItems[1]).css("transform", "none");
      $(activeItems[2]).css("transform", "scale(0.8)");
      $(activeItems[2]).css("filter", "grayscale(1)");
      $(activeItems[2]).css("opacity", "0.5");
    }
  });
});
$(".project-fullscreen").on("click", function (e) {
  //Default mouse Position
  let containingElement = document.querySelector(".project-fullscreen-image");
  if (containingElement.contains(e.target)) {
    // do nothing, click was inside container
  } else {
    $(e.target).css("display", "none");
    // hide autocomplete, click was outside container.
  }
});

document.addEventListener("keydown", function (event) {
  if (event.key == "ArrowLeft") {
    imageId -= 1;
    if (imageId == -1) {
      imageId = images.length - 1;
    }
    let imgName = images[imageId].replaceAll("-", " ").split(".")[0];
    imgName = imgName.charAt(0).toUpperCase() + imgName.slice(1);
    $(".project-fullscreen-text").html(`${imgName}`);
    $(".project-fullscreen-real-image").attr(
      "src",
      `./images/projekti/${images[imageId]}`
    );
  }
  if (event.key == "ArrowRight") {
    imageId += 1;
    if (imageId == images.length) {
      imageId = 0;
    }
    let imgName = images[imageId].replace("-", " ").split(".")[0];
    imgName = imgName.charAt(0).toUpperCase() + imgName.slice(1);
    $(".project-fullscreen-text").html(`${imgName}`);
    $(".project-fullscreen-real-image").attr(
      "src",
      `./images/projekti/${images[imageId]}`
    );
  }

  if (event.key == "Escape") {
    $(".project-fullscreen").css("display", "none");
  }
});

var navElement = document.querySelector("nav");
$(document).on("click", function (e) {
  if (!navElement.contains(e.target)) {
    $("#navbarNavAltMarkup").collapse("hide");
  }
});


window.addEventListener("load", function () {
  var iframe = document.getElementById("map-iframe").contentWindow.document;

  // Modify the pin color
  iframe.querySelector(".map-marker").style.filter = "none";
});