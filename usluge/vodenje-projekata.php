<base href="../" />
<?php
$title = "Vođenje građevinskih projekata | Fort-ing";
include '../header.php';

?>

<div class="services-hero padding-20" style="background-image: url('./images/usluge/vodenje-projekata.jpg')">
    <h1 class="text-white">Vođenje projekata</h1>
    <h2 class="text-primary ">Usluge</h2>
</div>

<div class="container-fluid">
<div class="bricks bricks-usluge d-none d-lg-block">
    <svg width="458" height="700" viewBox="0 0 458 986" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 230.689 646.635)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 295.175 695.824)" fill="#ED1E1E" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 292.175 695.824)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 231.372 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 222.765 942.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 288.759 794.202)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 234.105 843.392)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 289.443 892.581)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 109.082 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 464.402 450.122)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 409.747 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 525.888 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 465.085 548.5)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 402.915 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 348.261 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 464.402 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 403.599 794.447)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 522.472 597.689)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 523.156 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 288.141 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 404.282 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 343.478 646.879)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 197)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 344.741 246.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.937 295.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 221.767 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 167.113 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 222.451 541.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 286.67 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 402.811 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 342.008 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 161.647 295.379)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 106.993 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 223.134 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 165.33 393.757)" fill="#ED1E1E" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 162.33 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 519.689 0)" fill="#ED1E1E" stroke="#ED1E1E" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 465.034 49.1893)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 520.372 98.3784)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 458.202 245.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 403.547 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 519.689 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 458.885 344.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 523.105 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 343.427 147.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 398.765 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
    </svg>

</div>
    <div class="services-description push-top-25">
        <h2>Pametno upravljanje za najbolji rezultat:</h2>
        <h3 class="text-primary push-bottom-25">Vođenje projekata</h3>
        <p>Učinkovito vođenje građevinskih projekata ključno je za postizanje uspjeha u izgradnji. Naš stručni tim pruža
            sveobuhvatnu podršku od početka do kraja vašeg građevinskog projekta, osiguravajući stručno vođenje u svakom
            koraku. Koristimo najbolje prakse upravljanja projektima, koordiniramo resurse, nadziremo napredak i
            osiguravamo
            usklađenost s vremenskim rokovima i proračunom. S našim stručnjacima na vašoj strani, možete biti sigurni da
            će
            vaš građevinski projekt biti uspješno vođen, ispunjavajući visoke standarde kvalitete i postižući vaše
            ciljeve. </p>

        <p> Usluga vođenja projekata uključuje:
        <ul>
            <li><span>Ostvarivanje rezultata cijelog projekta i koordinaciju projektnog tima</li></span>
            <li><span>Upravljanje projektom, planiranje i koordinacija provedbe projekta i pojedinih aktivnosti</li>
            </span>
            <li><span>Praćenje napretka projekta prema predviđenim aktivnostima</li></span>
            <li><span>Mjesečna analiza predviđenih aktivnosti i ostvarenja definiranih ciljeva</li></span>
            <li><span>Redovito izvještavanje o postignutim rezultatima, izrada internih i financijskih izvješća</li>
            </span>
            <li><span>Definiranje i nadzor kvalitativnih i kvantitativnih pokazatelja uspješnosti projekta</li></span>
            <li><span>Komunikacija s ugovornim tijelom</li></span>
            <li><span>Upravljanje partnerskim odnosima</li></span>
            <li><span>Motivacija i edukacija suradnika te praćenje organizacijske klime i radne uspješnosti</li></span>
            <li><span>Drugi poslovi po nalogu poslodavca</li></span>
        </ul>
        </p>
        <p>
            Uz Fort-Ing, vaš projekt će biti vođen stručnom rukom. Naš tim posvećen je osiguravanju besprijekornog
            vođenja
            građevinskih projekata, postavljajući najviše standarde kvalitete i uspjeha. Stupite u kontakt s nama kako
            bismo
            raspravili o vašem projektu i pružili vam potrebnu podršku u ostvarenju vaših ciljeva.
        </p>

        <a href="kontakt.php">
            <button class="index-contact-button btn btn-primary primary border-primary push-top-50 text-primary">Kontaktirajte
                nas&nbsp;&nbsp;<i class="bi bi-chevron-right"></i></button>
        </a>
    </div>
</div>
<?php
include '../footer.php';
?>
<script>
$(document).ready(function() {
    $('a[href^="usluge/vodenje-projekata.php"]').addClass('active');
    $('a[href^="#"]').addClass('active');
});
</script>