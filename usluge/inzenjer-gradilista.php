<base href="../" />
<?php
$title = "Inženjer gradilišta | Fort-ing";
include '../header.php';

?>


<div class="services-hero padding-20" style="background-image: url('./images/usluge/inzenjer-gradilista.jpg')">
    <h1 class="text-white">Inženjer gradilišta</h1>
    <h2 class="text-primary ">Usluge</h2>
</div>
<div class="container-fluid">
    <div class="bricks bricks-usluge d-none d-lg-block">
        <svg width="458" height="700" viewBox="0 0 458 986" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 230.689 646.635)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 295.175 695.824)"
                fill="#ED1E1E" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 292.175 695.824)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 231.372 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 222.765 942.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 288.759 794.202)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 234.105 843.392)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 289.443 892.581)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 109.082 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 464.402 450.122)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 409.747 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 525.888 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 465.085 548.5)"
                stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 402.915 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 348.261 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 464.402 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 403.599 794.447)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 522.472 597.689)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 523.156 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 288.141 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 404.282 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 343.478 646.879)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 197)"
                stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 344.741 246.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 283.937 295.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 221.767 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 167.113 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 283.254 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 222.451 541.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 286.67 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 402.811 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 342.008 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 161.647 295.379)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 106.993 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 223.134 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 165.33 393.757)" fill="#ED1E1E" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 162.33 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 519.689 0)"
                fill="#ED1E1E" stroke="#ED1E1E" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 465.034 49.1893)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 520.372 98.3784)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 458.202 245.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 403.547 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 519.689 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 458.885 344.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 523.105 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 343.427 147.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 398.765 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        </svg>

    </div>
    <div class="services-description push-top-25">
        <h2>Rukovođenje za postizanje kvalitete najvišeg stupnja: </h2>
        <h3 class="text-primary push-bottom-25">Inženjer gradilišta</h3>
        <p>Naša usluga inženjera gradilišta pruža stručno vođenje i nadzor tijekom cijelog procesa izgradnje.
            Osiguravamo
            dosljednu usklađenost s projektom, tehničkim specifikacijama i relevantnim propisima. Nadziremo napredak
            radova,
            jamčimo kvalitetu izvedbe i rješavamo potencijalne izazove kako bismo osigurali uspješno i sigurno izvršenje
            projekta. Bez obzira na veličinu ili složenost projekta, naši inženjeri gradilišta pružaju stručnost i
            podršku
            potrebnu za ostvarenje vaših ciljeva.</p>
        <p>
            Usluga inženjera gradilišta uključuje:
        <ul>
            <li><span>Rukovođenje posla na gradilištu visokogradnje i niskogradnje u svojstvu glavnog izvođača </span>
            </li>
            <li><span>Izrada, kontrola i praćenje terminskih i financijskih planova </span></li>
            <li><span>Organiziranje i kontrola radova (kvaliteta, rokovi) svih vrsta građevinsko-obrtničkih i
                    instalaterskih
                    radova </span></li>
            <li><span>Razumijevanje i provođenje ugovorne, tehničke i projektne dokumentacije </span></li>
            <li><span>Izrada obračuna i financijsko praćenje učinka vlastitih radova i radova podizvođača </span></li>
            <li><span>Koordinacija i pravodobna komunikacija sa nadležnim odgovornim osobama investitora i nadzornog
                    inženjera </span></li>
            <li><span>Vođenje građevinskog dnevnika i građevinske knjige te izrada mjesečnih situacija investitoru za
                    izvedene radove </span></li>
            <li><span>Otklanjanje eventualnih nedostataka na građevini ili radovima po reklamacijama investitora i
                    korisnika
                    u jamstvenom roku </span></li>
        </ul>
        </p>
        <p>
            Uz Fort-Ing, vaš projekt će biti pod kontrolom iskusnog tima inženjera gradilišta. Naša predanost osigurava
            besprijekorno izvršenje građevinskih projekata, postavljajući najviše standarde kvalitete i postignuća.
            Javite
            nam
            se za detalje kako bismo uspješno realizirali svaku fazu vašeg projekta.
        </p>

        <a href="kontakt.php">
            <button class="index-contact-button btn btn-primary primary border-primary push-top-50 text-primary">Kontaktirajte
                nas&nbsp;&nbsp;<i class="bi bi-chevron-right"></i></button>
        </a>
    </div>
</div>
<?php
include '../footer.php';
?>
<script>
    $(document).ready(function () {
        $('a[href^="usluge/inzenjer-gradilista.php"]').addClass('active');
        $('a[href^="#"]').addClass('active');
    });
</script>