<?php
$title = "Kontakt | Fort-ing";
include 'header.php';

?>

<?php

require "vendor/autoload.php";


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;



$developmentMode = false;
$mailer = new PHPMailer($developmentMode);
if (!isset($msg)) {
    $msg = '';
}

if (isset($_POST["submit"]) && $_POST['med'] == "") {


    try {
        //$mailer->SMTPDebug = 2;
        $mailer->isSMTP();

        if ($developmentMode) {
            $mailer->SMTPOptions = [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ]
            ];
        }


        $mailer->Host = 'mail.forting.hr';
        $mailer->SMTPAuth = true;
        $mailer->Username = 'info@forting.hr';
        $mailer->Password = 'forting123!';
        $mailer->SMTPSecure = 'tls';
        $mailer->Port = 587;
        $mailer->CharSet = "UTF-8";
        //forting.info@gmail.com

        $mailer->setFrom('info@forting.hr', "" . $_POST['name'] . " vam je poslao poruku!");
        $mailer->addAddress('info@forting.hr', "fort-ing");
        $mailer->addReplyTo($_POST['email'], $_POST['name']);
        $mailer->isHTML(false);
        $mailer->Subject = 'Poruka sa web stranice';
        $mailer->Body = "Korisnik " . $_POST['name'] . " vam je poslao poruku!\n\nPoruka: " . $_POST['message'] . "\n\nIzabrana usluga : " . $_POST['usluga'] . "\nTelefonski broj: " . $_POST['number'] . "\nMail za odgovor: " . $_POST['email'] . "";

        $mailer->send();
        $mailer->ClearAllRecipients();
        //echo "MAIL HAS BEEN SENT SUCCESSFULLY cenna";
        $msg = 'Message sent! Thanks for contacting us.';
    } catch (Exception $e) {
        //echo "EMAIL SENDING FAILED. INFO: " . $mailer->ErrorInfo;
        $msg = 'Sorry, something went wrong. Please try again later.';
    }
}
$_POST['email'] = null;
$_POST['name'] = null;
$_POST['message'] = null;
$_POST['usluga'] = null;
$_POST['number'] = null;

?>

<div class="container-fluid">
    <div class="contact-header">
        <h1 class=" push-top-75-small">Kontaktirajte nas</h1>
        <h2 class="text-primary">Obratite nam se s povjerenjem</h2>
    </div>
    <div class="bricks bricks-contact d-none d-lg-block push-top-50">
        <svg width="290" height="288" viewBox="0 0 290 388" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 0)"
                fill="#ED1E1E" stroke="#ED1E1E" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 228.599 49.1895)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 344.74 49.1895)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 283.937 98.3789)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 225.992 246)"
                stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 283.254 295.136)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 329.992 344.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 341.325 147.567)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 286.67 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 345.008 245.946)"
                fill="#ED1E1E" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 342.008 245.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 106.992 147.567)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5"
                transform="matrix(-1 0 0 1 162.33 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        </svg>


    </div>

    <div class="form-container row">
        <div class="form-info col-sm-12 col-md-6 col-lg-6">
            <form method="post">
                <input type="text" id="med" name="med" style="display:none" value="">
                <div class="form-group">
                    <label for="name">Ime i Prezime</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Vaše ime i prezime">
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="imeprezime@email.com">
                </div>
                <div class="form-group">
                    <label for="number">Broj telefona</label>
                    <input type="text" class="form-control" id="number" name="number" placeholder="+3859933022">
                </div>
                <label for="usluga">Odaberite uslugu</label>
                <select class="form-control" id="usluga" name="usluga" id="exampleFormControlSelect1">
                    <option>Nadzor građenja</option>
                    <option>Inžinjer gradilišta</option>
                    <option>Projektiranje</option>
                    <option>Izrada energetskog certifikata</option>
                    <option>Vođenje projekata</option>
                </select>
                <div class="form-group">
                    <label for="message">Poruka</label>
                    <textarea class="form-control" id="message" name="message" rows="3"
                        placeholder="Vaša poruka"></textarea>
                </div>

                <button type="submit" name="submit" id="contact-submit"
                    class="push-top-25 btn btn-primary primary border-primary">Pošalji upit</button>
            </form>
            <p class="form--message">
                <?php echo $msg; ?>
            </p>

        </div>
        <div class="form-details col-sm-12 col-md-6 col-lg-6">
            <h4 class="push-top-25">Fort-ing j.d.o.o</h4>
            <div class="contact-flex">
                <p>
                    Vukovarska 50,
                    <br>
                    51265 Dramalj, Croatia
                    <br>
                    OIB: 842090544678
                </p>
                <p>
                    Domagoj Domijan, dipl. ing. građ.
                    <br>
                    Email: <a href="mailto:info@forting.hr">info@forting.hr</a>
                    <br>
                    Telefon: <a href="tel:+385 (0)98 407 636">+385 (0)98 407 636 </a>
                </p>
            </div>
            <div class="form-map-container ">
                <iframe class="form-map" id="map-iframe"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11248.695582197019!2d14.6916997!3d45.183576!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4717cf06278a2da5%3A0x2e9c45599598382c!2sFort-ing%20j.d.o.o.!5e0!3m2!1shr!2shr!4v1686857157798!5m2!1shr!2shr"
                    width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>
</div>








<?php
include 'footer.php';
?>

<script>
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
    $(document).ready(function () {

        $('a[href^="kontakt.php"]').addClass('active');
    });
</script>