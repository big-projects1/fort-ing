<?php
$title = "Fort-ing | Pouzdan partner u izgradnji i stručnom nadzoru";
include 'header.php';

?>

<div class="hero padding-10">
    <div class="hero-title">
        <h1 class="text-white hero-fort-ing">FORT-ING<br>građenje i nadzor</h1>
        <h2 class="text-white">Stručnost koja gradi povjerenje</h2>
    </div>
    <div class="hero-buttons push-bottom-25">
        <a href="#carousel" class="btn btn-usluge btn-primary primary border-primary">Naše usluge<i class="bi bi-chevron-right"></i></a>
        <a class="btn btn-projekti text-white border-white" href="projekti.php">Projekti</a>
    </div>
</div>

<div>
    <div class="fast-contact-white">
        <div class="row row-container">
            <div class="col-xs-12 col-s-12 col-md-6 col-lg-6 fast-contact-info-container">
                <div class="fast-contact-info">
                    <div class="fast-contact-icon">
                        <i class="text-white bi bi-envelope-fill"></i>
                    </div>
                    <div class="fast-contact-txt">
                        <p class="margin-none">Domagoj Domijan, dipl. ing. građ.<br>
                        <a href="mailto:info@forting.hr">info@forting.hr</a>  <br>
                        <a href="tel:+385 (0)98 407 636">+385 (0)98 407 636 </a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-s-12 col-md-6 col-lg-6 fast-contact-info-container">
                <div class="fast-contact-info">
                    <div class="fast-contact-icon">
                        <i class="text-white bi bi-geo-alt-fill"></i>
                    </div>
                    <div class="fast-contact-txt">
                        <p class="margin-none">Vukovarska 50<br>
                            51265 Dramalj, <br>
                            Croatia</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="index-fast-contact">
    </div>
</div>



<div class="bricks d-none d-lg-block">
    <svg width="458" height="700" viewBox="0 0 458 986" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 230.689 646.635)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 295.175 695.824)" fill="#ED1E1E" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 292.175 695.824)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 231.372 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 222.765 942.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 288.759 794.202)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 234.105 843.392)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 289.443 892.581)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 109.082 745.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 464.402 450.122)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 409.747 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 525.888 499.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 465.085 548.5)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 402.915 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 348.261 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 464.402 745.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 403.599 794.447)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 522.472 597.689)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 523.156 696.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 288.141 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 404.282 597.69)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 343.478 646.879)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 197)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 344.741 246.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.937 295.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 221.767 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 167.113 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 283.254 492.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 222.451 541.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 286.67 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 402.811 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 342.008 442.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 161.647 295.379)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 106.993 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 223.134 344.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 165.33 393.757)" fill="#ED1E1E" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 162.33 393.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 519.689 0)" fill="#ED1E1E" stroke="#ED1E1E" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 465.034 49.1893)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 520.372 98.3784)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 458.202 245.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 403.547 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 519.689 295.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 458.885 344.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 523.105 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 343.427 147.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
        <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 398.765 196.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
    </svg>

</div>

<div class="container-fluid">
    <div class="intro-container push-bottom-section">
        <h2>Tim stručnjaka koji izgrađuje vaše snove</h2>
        <h3 class="text-primary push-bottom-25">Otkrijte Fort-Ing</h3>
        <p>FORT-ING j.d.o.o. mlada je tvrtka je koja se bavi stručnim nadzorom, vođenjem projekata i projektiranjem u
            građevinarstvu. Tim čine iskusni inženjeri građevinarstva, arhitekture, strojarstva, elektrotehnike i
            geodezije,
            pokrivajući svojim iskustvom i područja izvan osnovne djelatnosti tvrtke.
            <br>
            <br>
            Inženjeri nisu samo na papiru, već imaju dugogodišnju terensku praksu u građenju raznih objekata od velikih
            investicija poput Hidroelektrane i Hotela pa sve do jednostavnih obiteljskih kuća i to ne samo u
            projektiranju i
            nadziranju, već i u izvođenju kao glavni inženjeri gradilišta.
            Stručnjaci sa iskustvom u vođenju projekata, projektiranju, nadzoru i vođenju gradilišta pružaju Vam uslugu
            potpunog
            vođenja i nadzora Vaše investicije.
            <br>
            <br>
            Objedinite potrebne radnje u pripremi, vođenju, nadzoru i finalizaciji Vašeg projekta, te tako smanjite Vaše
            troškove i potrebno vrijeme. Imajte kontrolu nad Vašim projektom. Bez obzira jeste li u pripremi projekta,
            ili
            već u
            finalizaciji, slobodno nam se obratite za inženjerske stručne savjete kao i pravnu pomoć te savjete pri
            ishođenju
            potrebnih dozvola za gradnju, etažiranja i dozvola za obavljanje djelatnosti.
        </p>
    </div>



    <div id="carousel" class="carousel-container push-top-75 push-bottom-section">
        <h2>Od temelja do krova</h2>
        <h3 class="text-primary push-bottom-25">Upoznajte naš širok spektar usluga za sve faze <br> građevinskih
            projekata
        </h3>
        <div class="owl-carousel owl-carousel-index owl-theme push-top-50">
            <div class="item">
                <div class="card align-items-stretch">
                    <img class="card-img-top card-img-top-index" src="images/usluge/inzenjer-gradilista.jpg" alt="Inženjer gradilišta">
                    <div class="card-body card-body-index">
                        <h4 class="card-title">Inženjer gradilišta</h4>
                        <p class="card-text">Uz Fort-Ing, vaš projekt će biti pod kontrolom iskusnog tima inženjera
                            gradilišta. Naša
                            predanost osigurava besprijekorno izvršenje građevinskih projekata, postavljajući
                            najviše standarde kvalitete i postignuća.</p>
                        <a class="btn btn-primary primary border-primary carousel-button" href="usluge/inzenjer-gradilista.php">Saznaj više...</a>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="card align-items-stretch">
                    <img class="card-img-top card-img-top-index" src="images/usluge/nadzor-gradenja.jpg" alt="Nadzor građenja">
                    <div class="card-body card-body-index">
                        <h4 class="card-title">Nadzor građenja</h4>
                        <p class="card-text">Naša predanost
                            osigurava da vaš projekt bude izveden u skladu s vašim očekivanjima i zahtjevima, uz
                            transparentno izvještavanje i provođenje potrebnih ispitivanja. Pružamo
                            sveobuhvatnu podršku tijekom svih faza građevinskih radova. </p>
                        <a class="btn btn-primary primary border-primary carousel-button" href="usluge/nadzor-gradenja.php">Saznaj više...</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="card align-items-stretch">
                    <img class="card-img-top card-img-top-index" src="images/usluge/projektiranje.jpg" alt="Projektiranje">
                    <div class="card-body card-body-index">
                        <h4 class="card-title">Projektiranje</h4>
                        <p class="card-text">Od početnog koncepta do detaljne dokumentacije,
                            omogućujemo ostvarenje vaše vizije na najbolji mogući način. Uz napredne tehnologije i
                            duboko razumijevanje industrije, možemo vam
                            pružiti sigurnost da će vaš projekt biti temeljito projektiran, optimiziran i usklađen sa
                            svim relevantnim propisima.</p>
                        <a class="btn btn-primary primary border-primary carousel-button" href="usluge/projektiranje.php">Saznaj više...</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="card align-items-stretch">
                    <img class="card-img-top card-img-top-index" src="images/usluge/izrada-certifikata.jpg" alt="Izrada energetskog certifikata">
                    <div class="card-body card-body-index">
                        <h4 class="card-title">Izrada energetskog certifikata
                        </h4>
                        <p class="card-text">Naš tim ovlaštenih stručnjaka detaljno će pregledati vašu nekretninu i
                            provesti sve
                            potrebne analize kako bi osigurao točnu procjenu energetske učinkovitosti. Naš cilj je
                            pružiti vam sveobuhvatan i pouzdan certifikat koji će vam pomoći u donošenju
                            informiranih odluka o energetskoj potrošnji. </p>
                        <a class="btn btn-primary primary border-primary carousel-button" href="usluge/izrada-certifikata.php">Saznaj više...</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="card align-items-stretch">
                    <img class="card-img-top card-img-top-index" src="images/usluge/vodenje-projekata.jpg" alt="Vođenje građevinskih projekata">
                    <div class="card-body card-body-index">
                        <h4 class="card-title">Vođenje projekata</h4>
                        <p class="card-text">Učinkovito vođenje građevinskih projekata ključno je za postizanje uspjeha
                            u izgradnji.
                            Naš stručni tim pruža sveobuhvatnu podršku od početka do kraja vašeg građevinskog
                            projekta, osiguravajući stručno vođenje u svakom koraku.</p>
                        <a class="btn btn-primary primary border-primary carousel-button" href="usluge/vodenje-projekata.php">Saznaj više...</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="projects-index-container push-top-75 push-bottom-75">
        <div class="bricks-projects">
            <svg width="591" height="750" viewBox="0 0 591 1031" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 228.599 691.635)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 293.086 740.824)" fill="#ED1E1E" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 290.086 740.824)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 229.283 790.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 220.676 987.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 286.67 839.202)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 232.015 888.392)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 287.353 937.581)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 106.993 790.013)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 465.312 495.122)" fill="#ED1E1E" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 462.312 495.122)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 407.658 544.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 523.799 544.311)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 462.995 593.5)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 400.826 741.068)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 346.171 790.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 462.312 790.257)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 401.509 839.446)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 286.051 642.689)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 402.192 642.689)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 341.389 691.879)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 281.164 242)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 342.651 291.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 281.848 340.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 219.678 487.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 165.023 537.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 281.164 537.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 220.361 586.324)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 284.58 438.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 400.722 438.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 339.918 487.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 159.558 340.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 221.044 389.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 163.241 438.757)" fill="#ED1E1E" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 160.241 438.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 284.647 45)" fill="#ED1E1E" stroke="#ED1E1E" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 229.993 94.1892)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 346.134 94.1892)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 285.331 143.378)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 456.106 290.946)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 401.451 340.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 517.592 340.135)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 456.789 389.325)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 462.895 0)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 521.008 241.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect width="109.993" height="43.0406" rx="15" transform="matrix(-1 0 0 1 409.993 143)" fill="#ED1E1E" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 406.993 143)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 587.549 600)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 532.895 649.189)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 517.895 441)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="60.4104" y="145.5" width="106.993" height="40.0406" rx="13.5" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="115.065" y="194.689" width="106.993" height="40.0406" rx="13.5" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="294.41" y="48.5" width="106.993" height="40.0406" rx="13.5" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 341.33 192.568)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
                <rect x="-1.5" y="1.5" width="106.993" height="40.0406" rx="13.5" transform="matrix(-1 0 0 1 396.668 241.757)" stroke="#ED1E1E" stroke-opacity="0.6" stroke-width="3" />
            </svg>

        </div>

        <h2>Građevinski projekti koji ostavljaju trag</h2>
        <h3 class="text-primary">Fort-ing kreira prostor s vizijom</h3>
        <p>U našim projektima kombiniramo najnovije tehnologije, vrhunske materijale i inovativne dizajne kako bismo stvorili prostor koji je funkcionalan, estetski privlačan i održiv. 
            Naša stručna ekipa arhitekata, inženjera i izvođača radova posvećena je pružanju vrhunskog kvaliteta i udovoljavanju vašim potrebama i očekivanjima. <br><br>

            Pregledajte naše projekte kako biste dobili uvid u našu stručnost i raznolikost našeg rada. Bilo da planirate izgradnju novog doma, poslovnog prostora ili obnovu postojeće nekretnine, 
            Fort-ing je pouzdan partner koji će vam pomoći u ostvarivanju vaših građevinskih ciljeva. Kontaktirajte nas danas i zajedno ćemo stvoriti prostor koji odražava vašu viziju i podržava zelenu budućnost.</p>
        <a class="index-contact-button btn btn-primary white text-primary border-primary projects-index-button" href="projekti.php">Naši projekti&nbsp;&nbsp;<i class="bi bi-chevron-right"></i></a>
    </div>
</div>



<div class="vision-container">
    <div class="row-vision-line d-none d-md-block"></div>
    <div class="row container-fluid vision-row push-top-75 push-bottom-section">
        <div class="col-xs-12 col-s-12 col-md-4 col-lg-4 vision-col">
            <div class="vision-card">
                <div class="vision-icon-container">
                    <img class="vision-icon" src="./images/vizija-misija-ciljevi/target.png" alt="Ikona misija" srcset="">
                </div>
                <div class="vision-txt">
                    <h3>Misija</h2>
                        <p>Naša misija je graditi snažne temelje za budućnost. Vjerujemo da je svaki projekt prilika
                            za
                            stvaranje
                            nečega
                            izvanrednog. Naša strast je pretvoriti ideje u stvarnost i oblikovati urbane pejzaže
                            koji
                            traju
                            generacijama.
                        </p>
                </div>
            </div>
        </div>
        <div class="vision-line d-sm-block d-md-none"></div>
        <div class="col-xs-12 col-s-12 col-md-4 col-lg-4 vision-col">
            <div class="vision-card">
                <div class="vision-icon-container">
                    <img class="vision-icon" src="./images/vizija-misija-ciljevi/opportunity.png" alt="Ikona vizija" srcset="">
                </div>
                <div class="vision-txt">
                    <h3>Vizija</h2>
                        <p>Naša vizija je biti pokretač pozitivnih promjena. S naprednim tehnikama i postavljanjem
                            novih
                            standarda,
                            težimo
                            inovacijama i održivom razvoju u zajednici u kojoj djelujemo kako bismo
                            ostavili pozitivan utjecaj na okolinu.
                        </p>
                </div>
            </div>
        </div>
        <div class="vision-line d-sm-block d-md-none"></div>
        <div class="col-xs-12 col-s-12 col-md-4 col-lg-4 vision-col">
            <div class="vision-card">
                <div class="vision-icon-container">
                    <img class="vision-icon" src="./images/vizija-misija-ciljevi/goal.png" alt="Ikona ciljevi" srcset="">
                </div>
                <div class="vision-txt">
                    <h3>Ciljevi</h2>
                        <p>Nastojimo postići izvrsnost u svakom aspektu našeg poslovanja. Zadovoljstvo klijenata,
                            održivost
                            i
                            inovacija
                            temelji su našeg poslovanja, kako bismo ostvarili dugoročne i odgovorne građevinske
                            projekte.
                        </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="index-contact-container container-fluid push-top-75 push-bottom-75">
    <h3 class="push-bottom-25">Fort-Ing pruža stručne inženjerske savjete, pravnu pomoć i brzo ishođenje dozvola.
        Uz
        nas,
        optimizirajte troškove,
        ubrzajte izvedbu i postignite izvanredne rezultate!
    </h3>
    <a class="index-contact-button btn btn-primary primary border-primary text-primary" href="kontakt.php">Kontaktirajte
        nas&nbsp;&nbsp;<i class="bi bi-chevron-right"></i></a>
</div>

<!-- c-fluid -->







<?php
include 'footer.php';
?>
<script>
    $(document).ready(function() {
        $('a[href^="index.php"]').addClass('active');
    });
</script>