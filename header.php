<!doctype html>
<html lang="en">

<head>
    <title>

        <?php
        echo $title;
        ?>
    </title>


    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "Fort-ing j.d.o.o.",
            "description": "FORT-ING je mlada tvrtka specijalizirana za stručni nadzor, vođenje projekata i projektiranje u građevinarstvu koju čini tim inženjera građevine, arhitekture, strojarstva, elektrotehnike i geodezije. Pružamo potpunu uslugu vođenja, nadzora i projektiranja za vašu investiciju, bilo da se radi o velikim ili manjim projektima.",
            "image": "https://www.forting.hr/images/logo-fort-ing-new.png",
            "logo": "https://www.forting.hr/images/logo-fort-ing-new.png",
            "url": "https://www.forting.hr/",
            "telephone": "+385-98-407-636",
            "sameAs": [
                "https://www.companywall.hr/tvrtka/fort-ing-jdoo/MMS41KpR"
            ],
            "address": {
                "@type": "PostalAddress",
                "streetAddress": " Vukovarska 50",
                "addressLocality": "Dramalj",
                "postalCode": "51265",
                "addressCountry": "Croatia"
            },
            "contactPoint": [{
                "@type": "ContactPoint",
                "telephone": "+385-98-407-636",
                "contactType": "customer support",
                "areaServed": ["HR", "150"],
                "availableLanguage": ["English", "Croatian"]
            }, {
                "@type": "ContactPoint",
                "telephone": "+385-98-407-636",
                "contactType": "customer support",
                "areaServed": ["HR", "150"],
                "availableLanguage": ["English", "Croatian"]
            }]
        }
    </script>

    <link rel="icon" id="faviconTag" type="image/svg" href="images/favicon.svg">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="language" content="hr" />



    <!-- meta data -->

    <meta name="description" content="FORT-ING je mlada tvrtka specijalizirana za stručni nadzor, vođenje projekata i projektiranje u građevinarstvu koju čini tim inženjera građevine, arhitekture, strojarstva, elektrotehnike i geodezije. Pružamo potpunu uslugu vođenja, nadzora i projektiranja za vašu investiciju, bilo da se radi o velikim ili manjim projektima." />
    <meta name="keywords" content="Fort-ing, građevinska firma, stručni nadzor građenja, vođenje projekata, projektiranje, građevinarstvo, inženjer gradilišta, izrada energetskog certifikata" />

    <meta name="author" content="Fort-ing" />
    <meta name="copyright" content="Fort-ing" />

    <!-- facebook meta data -->
    <meta property="og:locale" content="hr_HR" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Fort-ing | Pouzdan partner u izgradnji i građevinskom nadzoru" />
    <meta property="og:description" content="FORT-ING je mlada tvrtka specijalizirana za stručni nadzor, vođenje projekata i projektiranje u građevinarstvu koju čini tim inženjera građevine, arhitekture, strojarstva, elektrotehnike i geodezije. Pružamo potpunu uslugu vođenja, nadzora i projektiranja za vašu investiciju, bilo da se radi o velikim ili manjim projektima." />
    <meta property="og:site_name" content="Fort-ing" />
    <!-- Website url-->
    <meta property="og:url" content="https://www.forting.hr/" />


    <!-- twitter meta data -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:domain" content="Fort-ing_construction" />
    <!-- URL vaše web stranice -->
    <meta name="twitter:url" content="https://www.forting.hr/" />
    <meta name="twitter:title" content="Fort-ing | Pouzdan partner u izgradnji i građevinskom nadzoru" />
    <meta name="twitter:description" content="FORT-ING je mlada tvrtka specijalizirana za stručni nadzor, vođenje projekata i projektiranje u građevinarstvu koju čini tim inženjera građevine, arhitekture, strojarstva, elektrotehnike i geodezije. Pružamo potpunu uslugu vođenja, nadzora i projektiranja za vašu investiciju, bilo da se radi o velikim ili manjim projektima." />



    <!-- GeoLocation Meta Tags / Geotagging.-->
    <meta name="geo.placename" content="Fort-ing, Vukovarska 50, 51265, Dramalj, Croatia" />
    <meta name="geo.region" content="HR-Primorsko-goranska županija" />
    <meta name="geo.position" content="45.2;14.7" />
    <meta name="ICBM" content="45.2, 14.7" />



    <!-- Dublin Core Metadata Element Set.  -->
    <meta property="dcterms:title" content="Fort-ing | Pouzdan partner u izgradnji i građevinskom nadzoru" />
    <meta property="dcterms:description" content="FORT-ING je mlada tvrtka specijalizirana za stručni nadzor, vođenje projekata i projektiranje u građevinarstvu koju čini tim inženjera građevine, arhitekture, strojarstva, elektrotehnike i geodezije. Pružamo potpunu uslugu vođenja, nadzora i projektiranja za vašu investiciju, bilo da se radi o velikim ili manjim projektima." />
    <meta property="dc:subject" content="Građenje i stručni nadzor" />
    <meta property="dc:creator" content="Fort-ing" />
    <meta property="dc:publisher" content="Fort-ing" />
    <meta property="dc:format" content="text/html" />
    <meta property="dc:identifier" content="https://www.forting.hr/" />
    <meta property="dc:type" content="text/html" />

    <meta name="robots" content="index, follow" />


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">

    <link rel="stylesheet" href="owlcarousel/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="owlcarousel/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/construction.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-md.css">
    <link rel="stylesheet" href="css/style-lg.css">
</head>

<body>
    <nav class="navbar-container fixed-top navbar navbar-expand-lg navbar-light">

        <a class="navbar-brand" href="index.php">
            <img class="navbar-logo" src="" alt="Logo Fort-ing" height="90">
        </a>


        <button class="navbar-toggler navbar-toggler-left" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <i class="bi bi-list navbar-hamburger"></i>
        </button>
        <a class="ml-auto nav-phone text-white d-lg-inline order-last" href="tel:+385 (0)98 407 636">
            <i class="bi bi-telephone-fill text-white"></i> +385 (0)98 407 636
        </a>

        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <li class="nav-item"> <a class="nav-link text-white" href="index.php">O nama</a></li>
                <li class="nav-item dropdown nav-list">
                    <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Usluge
                    </a>
                    <ul class="dropdown-menu nav-dropdown" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="usluge/nadzor-gradenja.php">Nadzor građenja</a></li>
                        <li><a class="dropdown-item" href="usluge/vodenje-projekata.php">Vođenje projekata</a></li>
                        <li><a class="dropdown-item" href="usluge/projektiranje.php">Projektiranje</a></li>
                        <li><a class="dropdown-item" href="usluge/inzenjer-gradilista.php">Inženjer gradilišta</a></li>
                        <li><a class="dropdown-item" href="usluge/izrada-certifikata.php">Izrada energetskog certifikata</a></li>
                    </ul>
                </li>
                <li class="nav-item"> <a class="nav-link text-white" href="projekti.php">Projekti</a></li>
                <li class="nav-item"> <a class="nav-link text-white" href="kontakt.php">Kontakt</a></li>
            </div>
        </div>


    </nav>